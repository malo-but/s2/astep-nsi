import pygame

class Slot:
    
    def __init__(self, color, size, pos) -> None:
        
        self.color = color
        
        self.size : tuple[int, int] = size
        
        self.pos : tuple[int, int] = pos
        
        self.rect = pygame.Rect(self.pos[0], self.pos[1], self.size[0], self.size[1])
        
        self.focused = False
        