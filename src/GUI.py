import pygame, random

from src.money import Money
from src.slot import Slot
from src.button import Button
from TP_Rendu_Monnaie import rendu_monnaie, prix_article
  
def run(bonus):
    
    cost = prix_article()
    
    if type(cost) != int:
        
        print("ERREUR : Le prix de l'article doit être un entier !")
        
        return
            
    pygame.init()
    pygame.display.set_caption("Cash Register")
    screen = pygame.display.set_mode((1200, 800))
    
    font = pygame.font.Font("assets/font.ttf", 30)
    
    amount = 0

    cursor = pygame.Rect(pygame.mouse.get_pos()[0], pygame.mouse.get_pos()[1], 1, 1)
    
    money = create_money()
    
    slot = Slot((255, 0, 0), (500, 150), (50, 25))
    
    button = Button(75, (675, 100))
    
    bills = [0 for i in range(9)]
            
    while handle_inputs(cursor, money, button, bonus):
        
        screen.fill((150, 150, 150))
        
        if button.was_pressed:
            
            screen_amount = pygame.Rect(800, 0, 400, screen.get_height())
            
            pygame.draw.rect(screen, (0, 0, 0), screen_amount)
            
            display_bills(screen, font, bills)
            
        else:
            
            if bonus:
                    
                screen_amount = pygame.Rect(800, 0, 400, 250)
                
            else:
            
                screen_amount = pygame.Rect(800, 0, 400, 160)
                
            pygame.draw.rect(screen, (0, 0, 0), screen_amount)
            
        display_cost = cost
        
        reduc = 0
            
        if bonus:
        
            copy_cost = cost
                    
            if copy_cost >= 100 :
                
                reduc += 10
                
                copy_cost -= 100
                
                while copy_cost >= 50 and reduc < 35 :
                    
                    reduc += 5
                    
                    copy_cost -= 50
                                    
            display_cost = round(display_cost - (display_cost * reduc / 100))
                        
        text_cost = font.render(f"COST : {cost}€", True, (0, 255, 0))
        text_paid = font.render(f"PAID : {amount}€", True, (0, 255, 0))
        
        due = amount - display_cost
        if due < 0:
            due = 0
            
        text_due = font.render(f"DUE : {due}€", True, (0, 255, 0))
        
        if bonus:
        
            text_reduc = font.render(f"REDUC : {reduc}%", True, (0, 255, 0))
            
            screen.blit(text_reduc, (825, 160))
            
            text_new_cost = font.render(f"NEW COST : {display_cost}€", True, (0, 255, 0))
            
            screen.blit(text_new_cost, (825, 210))
        
        text_pay = font.render("PAY", True, (0, 0, 0))
        
        screen.blit(text_cost, (825, 10))
        screen.blit(text_paid, (825, 60))
        screen.blit(text_due, (825, 110))
        
        pygame.draw.rect(screen, (0, 0, 0), slot.rect)
        
        pygame.draw.rect(screen, slot.color, slot.rect, 20)
        
        pygame.draw.circle(screen, (255, 0, 0), button.pos, button.radius)
        
        screen.blit(text_pay, (button.pos[0] - (button.radius / 2), button.pos[1] - (button.radius / 4)))
        
        display_money(screen)
        
        if slot.focused:
            
            pygame.draw.rect(screen, (0, 0, 255), slot.rect, 20)
            
        if button.pressed:
            
            pygame.draw.circle(screen, (0, 0, 255), button.pos, button.radius)
                        
            bills = rendu_monnaie(cost, amount)
            
        for coin in money:
            
            if coin.real and coin.dragged:
                                
                coin.pos = (pygame.mouse.get_pos()[0] - int(coin.width / 2), pygame.mouse.get_pos()[1] - int(coin.height / 2))
                
                slot.focused = pygame.Rect.colliderect(coin.get_rect(), slot.rect)
                    
            else:
                
                if pygame.Rect.colliderect(coin.get_rect(), slot.rect):
                    
                    amount += coin.value
                    
                    slot.focused = False
                    
                coin.back_to_base()
            
            screen.blit(coin.sprite, coin.pos)
            
        cursor.x = pygame.mouse.get_pos()[0]
        cursor.y = pygame.mouse.get_pos()[1]        
        
        pygame.display.flip()
        
def create_money():
    
    money = []
        
    for i in [1, 2, 5, 10, 20, 50, 100, 200, 500]:
        
        money.append(Money(i, True))
    
    return money

def handle_inputs(cursor , money, button, bonus):
    
    for event in pygame.event.get():
            
        if event.type == pygame.QUIT:
            return False
            
        if event.type == pygame.MOUSEBUTTONDOWN or event.type == pygame.MOUSEBUTTONUP:
            
            for coin in money:
                
                if coin.real and pygame.Rect.colliderect(cursor, coin.get_rect()):
                    
                    coin.dragged = not coin.dragged
                
            if pygame.Rect.colliderect(cursor, button.get_rect()):
                
                button.pressed = not button.pressed
                
                button.was_pressed = True
                
        if event.type == pygame.KEYDOWN:
            
            if event.key == pygame.K_ESCAPE:
                
                return False
            
            elif event.key == pygame.K_SPACE:
                
                pygame.quit()
                
                run(bonus)
                
                return False
                
                
    return True

def display_money(screen):
    
    e1 = Money(1, False)
    
    e2 = Money(2, False)
    
    e5 = Money(5, False)
    
    e10 = Money(10, False)
    
    e20 = Money(20, False)
     
    e50 = Money(50, False)
    
    e100 = Money(100, False)
    
    e200 = Money(200, False)
    
    e500 = Money(500, False)
    
    screen.blit(e1.sprite, e1.pos)
    
    screen.blit(e2.sprite, e2.pos)
    
    screen.blit(e5.sprite, e5.pos)
    
    screen.blit(e10.sprite, e10.pos)
    
    screen.blit(e20.sprite, e20.pos)
    
    screen.blit(e50.sprite, e50.pos)
    
    screen.blit(e100.sprite, e100.pos)
    
    screen.blit(e200.sprite, e200.pos)
    
    screen.blit(e500.sprite, e500.pos)
    
    
def display_bills(screen, font, bills):
    
    values = [500,200,100,50,20,10,5,2,1]
        
    for i in range(len(bills) - 1, -1, -1):
                
        text = font.render(f"{values[i]}€ : x{bills[i]}", True, (0, 255, 0))
        
        screen.blit(text, (825, 300 + (i * 50)))