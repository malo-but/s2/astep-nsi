import pygame

class Button:
    
    def __init__(self, radius, pos) -> None:
        
        self.radius = radius
        
        self.pos : tuple[int, int] = pos
        
        self.pressed = False
        
        self.was_pressed = False
        
    def get_rect(self):
        
        return pygame.Rect(self.pos[0] - self.radius, self.pos[1] - self.radius, self.radius * 2, self.radius * 2)