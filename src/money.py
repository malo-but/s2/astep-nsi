import pygame

class Money:
    
    def __init__(self, value, real):
        
        self.value = value
        
        letter = "e"
        
        num = value
        
        if value < 1:
            num = int(value * 100)
            letter = "c"
            
        asset = f"assets/{num}{letter}.png"
        
        # self.__base_positions = {0.01:(50, 700), 0.02:(150, 700), 0.05:(250, 700), 0.1:(350, 700), 0.2:(450, 700), 0.5:(550, 700), 1:(650, 700), 2:(750, 700), 5:(50, 550), 10:(250, 550), 20:(450, 550), 50:(650, 550), 100:(150, 400), 200:(350, 400), 500:(550, 400)}
        
        self.__base_positions = {1:(20, 650), 2:(140, 650), 5:(260, 650), 10:(460, 650), 20:(50, 520), 50:(260, 520), 100:(460, 520), 200:(50, 390), 500:(260, 390)}
        
        self.pos :tuple[int, int] = self.__base_positions[value]
        
        self.sprite = pygame.image.load(asset)
        
        self.dragged = False
        
        self.width = self.sprite.get_width()
        
        self.height = self.sprite.get_height()
        
        self.real = real
        
        
    def get_rect(self):
        
        return pygame.Rect(self.pos[0], self.pos[1], self.width, self.height)
    
    def back_to_base(self):
        
        self.pos = self.__base_positions[self.value]
        
        self.dragged = False