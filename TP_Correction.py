"""
PSEUDO-CODE :

fonction rendu_monnaie(cout_article, montant_donne) :

	nb_billets <- [0,0,0,0,0,0,0,0,0]
 
	billets <- [500,200,100,50,20,10,5,2,1]
 
	surplus <- montant_donne - cout_article
 
	i <- 0
 
	tant que surplus > 0 et i < longueur(billets) :
 
		si surplus >= billets[i] :
  
			nb_billets[i] <- nb_billets[i] + 1

			surplus <- surplus - billets[i]
   
		sinon :
  
			i <- i + 1
   
   renvoie nb_billets

"""

def rendu_monnaie(cout_article, montant_donne):
    
    # DEBUT BONUS
    
    copy_cout_article = cout_article
    
    reduc = 0
    
    if copy_cout_article >= 100 :
        
        reduc += 10
        
        copy_cout_article -= 100
        
        while copy_cout_article >= 50 and reduc < 35 :
            
            reduc += 5
            
            copy_cout_article -= 50
                        
    cout_article = round(cout_article - (cout_article * reduc / 100))
            
    # FIN BONUS   
    
    nb_billets = [0,0,0,0,0,0,0,0,0]
    
    billets = [500,200,100,50,20,10,5,2,1]
    
    surplus = (montant_donne - cout_article)
    
    i = 0
            
    while surplus > 0 and i < len(billets):
        
        if surplus >= billets[i] :
            
            nb_billets[i] += 1
            
            surplus -= billets[i]
        
        else :
            
            i += 1
            
    return nb_billets


rendu_monnaie(543, 200)