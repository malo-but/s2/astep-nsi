from src.GUI import *

if __name__ == "__main__":
    
    bonus = False
    
    run(bonus)
    
    
""" 

Fonction qui prend en paramètre le prix de l'article et le montant donné par le client
et qui retourne une liste contenant le nombre de billets à rendre. 
La liste doit être triée par ordre décroissant des valeurs des billets.

Ex : montant_a_rendre = 15€, donc nb_billets = [0, 0, 0, 0, 0, 1, 1, 0, 0]
    500€ : 0
    200€ : 0
    100€ : 0
    50€ : 0
    20€ : 0
    10€ : 1
    5€ : 1
    2€ : 0
    1€ : 0
    
"""
def rendu_monnaie(cout_article, montant_donne):
    
    # La liste des nombres de billets à rendre
    nb_billets = []
    
    # VOTRE CODE ICI
    
    return nb_billets


"""

Fonction qui détermine le prix de l'article passé en caisse.
Retourne un entier aléatoire par défaut.
A vous de la modifier (ou non) pour réaliser vos tests.
La fonction doit retourner un entier !!!

"""
def prix_article():
        
    return random.randint(1, 1001)